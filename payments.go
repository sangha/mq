package mq

import (
	"encoding/json"
	"log"
	"time"

	"github.com/streadway/amqp"
)

type Payment struct {
	Name    string
	Address []string
	City    string
	ZIP     string
	Country string
	Phone   string
	Email   string

	DateTime        time.Time
	Amount          int64
	AmountS         string
	Currency        string
	TransactionCode string
	Description     string

	Source              string
	SourceID            string
	SourcePayerID       string
	SourceTransactionID string

	BudgetID  int64
	PaymentID int64
}

func (p *Payment) Process() error {
	ch, err := GetAMQPChannel()
	if err != nil {
		return err
	}

	body, _ := json.Marshal(*p)
	err = ch.Publish(
		"",         // exchange
		"payments", // routing key
		false,      // mandatory
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Body:         []byte(body),
		})
	if err != nil {
		return err
	}

	log.Println("Sent receipt request")
	return nil
}
