package main

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/streadway/amqp"

	"gitlab.com/sangha/mq"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func main() {
	conn, err := amqp.Dial("amqp://quitty:quitty@10.0.3.135:5672/rabbit")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	i := int64(0)
	for {
		i = i + 123
		rcpt := mq.Receipt{
			Name:     "John Doe",
			Address:  []string{"1 Flat", "Some Street"},
			City:     "Some Town",
			ZIP:      "12345",
			DateTime: time.Now(),
			Value:    i,
		}
		body, _ := json.Marshal(rcpt)

		err = ch.Publish(
			"",         // exchange
			"receipts", // routing key
			false,      // mandatory
			false,
			amqp.Publishing{
				DeliveryMode: amqp.Persistent,
				ContentType:  "application/json",
				Body:         []byte(body),
			})
		failOnError(err, "Failed to publish a receipt request")
		log.Println("Sent receipt request")
		time.Sleep(1 * time.Second)
	}
}
