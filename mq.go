package mq

import (
	"gitlab.com/sangha/sangha/config"

	"github.com/streadway/amqp"
)

var (
	amqpConn   *amqp.Connection
	amqpChan   *amqp.Channel
	amqpConfig config.AMQPConnection
)

// SetupAMQP sets the AMQP configuration
func SetupAMQP(ac config.AMQPConnection) {
	amqpConfig = ac
}

// GetAMQPChannel returns an AMQP channel
func GetAMQPChannel() (*amqp.Channel, error) {
	var err error

	if amqpConn == nil {
		amqpConn, err = amqp.Dial(amqpConfig.Marshal())
		if err != nil {
			return nil, err
		}

		amqpChan, err = amqpConn.Channel()
		if err != nil {
			return nil, err
		}

		err = amqpChan.ExchangeDeclare(amqpConfig.Exchange, "direct",
			true, false, false, false, nil)
		if err != nil {
			panic(err)
			return nil, err
		}

		_, err = amqpChan.QueueDeclare("payments", true,
			false, false, false, nil)
		if err != nil {
			panic(err)
			return nil, err
		}

		err = amqpChan.QueueBind("payments", "payments", amqpConfig.Exchange, true, nil)
		if err != nil {
			panic(err)
			return nil, err
		}
	}

	return amqpChan, nil
}
