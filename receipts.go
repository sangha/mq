package mq

import (
	"math/big"
	"time"
)

type Receipt struct {
	Name    string
	Address []string
	City    string
	ZIP     string
	Country string
	Phone   string
	Email   string

	Value    *big.Float
	DateTime time.Time
}
